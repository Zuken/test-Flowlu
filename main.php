<html>
    <head>
        <title>Тестовое задание Flowlu</title>
        <link rel="stylesheet" href="css/main.css" type="text/css">
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

    </head>
    <body>
    <div id="wrapper" class="container">
        <div class="invisible" style="margin-top: 10%">
            <h2 id="result-validate" class="text-center">Результат:</h2>
        </div>
        <div id="div-form" class="w-100">
            <form id="input-form" method="post" class="form-row align-items-center">
                <input id="input-field" type="text" name="text" placeholder="Введите текст" class="form-control col">
                <input type="submit" id="button" value="Проверить" class="btn btn-success ">
            </form>
        </div>
        <h3 class="text-center mt-5" >История запросов</h3>
        <div id="div-history" class="justify-content-center">
            <div class="row mx-0 border">
                <div class="col-10" style="padding: 0.5rem">
                    <p class="text-center mb-0">Введенные значения</p>
                </div>
                <div class="col-2 border-left" style="padding: 0.5rem">
                    <p class="text-center mb-0">Результат</p>
                </div>
            </div>
            <div id="records">
                <div class="row mx-0">
                    <p class="text-center w-100">Записей нет</p>
                </div>
            </div>
        </div>
    </div>

    </body>
    <script>
        function updateHistory()
        {
            fetch('/history').then(response => {
                response.text().then(text => {
                    document.getElementById('records').innerHTML = text;
                });
            });
        }
    </script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            updateHistory();
        });
    </script>
    <script>
        let form = document.getElementById('input-form');
        let result = document.getElementById('result-validate');

        form.addEventListener('submit', function (evt) {
            evt.preventDefault();
            let value = document.getElementById('input-field').value;

            fetch('/brackets', {
                method: 'post',
                body: JSON.stringify({"text": value}),
                headers: {
                    'content-type': 'application/json'
                }
            }).then(response => {
                response.json().then(data => {
                    if (data['error'] != null) {
                        result.innerText = data['error'];
                        result.parentElement.className = 'visible';
                        document.getElementById('input-field').focus();
                    } else {
                        result.innerText = 'Результат: ' + data['resultValidate'];
                        result.parentElement.className = 'visible';
                    }
                });
                updateHistory();
            });
        });
    </script>
</html>
