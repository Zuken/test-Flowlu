<?php
    require_once 'Database.php';

    $uri = $_SERVER['REQUEST_URI'];
    if ($uri === '/') {
        require 'main.php';
    } elseif (($uri === '/brackets') && ($_SERVER['REQUEST_METHOD'] == 'POST')) {
        require 'brackets.php';
    } elseif ($uri === '/history') {
        require 'history.php';
    }
    else {
        require 'error.php';
    }
