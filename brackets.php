<?php
    $data = json_decode(file_get_contents("php://input"),true);
    if (isset($data['text'])) {
        $data['text'] = $data['text'].trim(" \n\r\t\v\0");
        if (!empty($data['text'])) {
            $fileName = 'Model.php';
            $filePath = dirname(__FILE__) . '/' . $fileName;
            if (!file_exists($filePath)) {
                echo json_encode(['success' => false, 'error' => "File $filePath is not found "]);
                return;
             }
            require $filePath;
            $model = new Model();

            $resultValidate = $model->brackets($data['text']);
            $resultQuery = Database::queryInsert("INSERT INTO history(inputted_value, result) VALUES (:inputted_value, :result)", ['inputted_value' => $data['text'], 'result' => $resultValidate ? 1 : 0]);
            echo json_encode(['success' => $resultQuery, 'resultValidate' => $resultValidate]);
            return;
        }
    }
    echo json_encode(['success' => false, 'error' => 'Поле не должно быть пустым']);
