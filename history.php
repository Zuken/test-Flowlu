<?php
    $result = Database::querySelect("SELECT * FROM history ORDER BY id DESC");
    if (is_array($result)) {
        if(isset($result['error']) && !empty($result['error'])) {
            echo '<div class="row mx-0 border border-top-0"><p class="text-center w-100 mt-3">' . $result['error'] . '</p></div>';
        }
    } elseif (($result === null) || ($result->rowCount() < 1)) {
        echo '<div class="row mx-0 border border-top-0"><p class="text-center w-100 mt-3">Записей нет</p></div>';
    } else {
        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
            echo
                '<div class="row mx-0 border">' .
                    '<div class="col-10">' .
                        '<p class="text-justify">' .
                            $row['inputted_value'] .
                        '</p>' .
                    '</div>' .
                    '<div class="col-2 border-left">' .
                        '<p class="text-center">';
                            echo $row['result'] ? 'True' : 'False';
                        echo
                        '</p>' .
                    '</div>' .
                '</div>';
        }
    }
