<?php
class Database
{
    private static string $host ='localhost';
    private static string $database = 'TestFlowlu';
    private static string $port = '5432';
    private static string $username = 'postgres';
    private static string $password = 'password';

    public static function createTable()
    {
        $connectionString = "pgsql:host=". self::$host .";port=" . self::$port . ";dbname=" . self::$database . ";user=" . self::$username . ";password=" . self::$password;
        try {
            $dbConnection = new PDO($connectionString);
            if($dbConnection) {
                $query = "CREATE TABLE IF NOT EXISTS History (
                            id serial NOT NULL,
                            inputted_value character varying NOT NULL,
                            result boolean NOT NULL,
                            PRIMARY KEY (id)
                        )";
                echo $dbConnection->query($query)->execute() ? 'Таблица создана' : 'Таблица не создана';
            }
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    public static function querySelect($query)
    {
        $connectionString = "pgsql:host=". self::$host .";port=" . self::$port . ";dbname=" . self::$database . ";user=" . self::$username . ";password=" . self::$password;
        $errorMessage = '';
        try {
            $dbConnection = new PDO($connectionString);
            if($dbConnection) {
                if ($dbConnection->query($query)->execute()) {
                    return $dbConnection->query($query);
                }
            }
        } catch (PDOException $e){
            $errorMessage = 'Не удалось выполнить запрос';
            return array('error' => $errorMessage);
        }
        return null;
    }

    public static function queryInsert($query, $params) : bool
    {
        $connectionString = "pgsql:host=". self::$host .";port=" . self::$port . ";dbname=" . self::$database . ";user=" . self::$username . ";password=" . self::$password;
        try {
            $dbConnection = new PDO($connectionString);
            if($dbConnection) {
                return $dbConnection->prepare($query)->execute($params);
            }
        } catch (PDOException $e){
            echo 'Не удалось выполнить запрос';;
        }
        return false;
    }
}
