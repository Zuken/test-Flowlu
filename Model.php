<?php
class Model
{
    public function brackets($string) : bool
    {
        $array = str_split($string);
        $resultIntersection = array_intersect($array, ['<', '>', '(', ')', '{', '}', '[', ']']); // Массив с совпадающими значениями
        if (!empty($resultIntersection)) {
            $brackets = ['>' => '<', ')' => '(', '}' => '{', ']' => '[']; // Соответсвие открытых и закрытых скобок
            $closingBrackets = array_keys($brackets);
            $openedBrackets = [];
            foreach ($resultIntersection as $bracket) {
                if (!in_array($bracket, $closingBrackets)) {
                    $openedBrackets[] = $bracket;
                    continue;
                }
                if (end($openedBrackets) != $brackets[$bracket]) return false;
                array_pop($openedBrackets);
            }
        }
        return true;
    }
}
